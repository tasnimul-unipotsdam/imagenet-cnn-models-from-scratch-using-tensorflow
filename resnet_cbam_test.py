import os
import tensorflow as tf



os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
devises = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(devises[0], True)

opts = {
    'kernel_initializer': tf.keras.initializers.he_uniform(),
    'bias_initializer': tf.keras.initializers.zeros()
}


import tensorflow as tf
import tensorflow.keras.backend as k

def _channel_attention_(inputs, ratio=8):
    channel = inputs.get_shape()[-1]

    shared_layer1 = tf.keras.layers.Dense(channel // ratio, activation="relu", kernel_initializer="he_normal", use_bias=True, bias_initializer="zeros")
    shared_layer2 =  tf.keras.layers.Dense(channel, activation="relu", kernel_initializer="he_normal", use_bias=True, bias_initializer="zeros")

    x1 = tf.keras.layers.GlobalAveragePooling2D()(inputs)
    x1 = tf.keras.layers.Reshape((1, 1, channel))(x1)
    x1 = shared_layer1(x1)
    x1 = shared_layer2(x1)

    x2 = tf.keras.layers.GlobalMaxPooling2D()(inputs)
    x2 = tf.keras.layers.Reshape((1, 1, channel))(x2)
    x2 = shared_layer1(x2)
    x2 = shared_layer2(x2)

    x = tf.keras.layers.add([x1, x2])
    x = tf.keras.layers.Activation("sigmoid")(x)

    x = tf.keras.layers.multiply([inputs, x])

    return x



def _spatial_attention_(inputs):
    kernel_size = 7

    x1 = tf.keras.layers.Lambda(lambda x: k.mean(x, axis=3, keepdims=True))(inputs)
    x2 = tf.keras.layers.Lambda(lambda x: k.max(x, axis=3, keepdims=True))(inputs)

    concat = tf.keras.layers.concatenate([x1, x2])
    conv = tf.keras.layers.Conv2D(1, kernel_size=kernel_size, strides=1, activation="sigmoid", padding="same", kernel_initializer="he_normal", use_bias=False)(concat)
    feature = tf.keras.layers.multiply([inputs, conv])
    return feature

def cbam_block(inputs, ratio=8):

    channel_attention = _channel_attention_(inputs, ratio)
    spatial_attention = _spatial_attention_(channel_attention)
    return spatial_attention




def _conv_block(x,
                filters,
                kernel_size,
                strides,
                use_bias=False,
                padding='same'):
    x = tf.keras.layers.Conv2D(filters=filters,
                               kernel_size=kernel_size,
                               strides=strides,
                               use_bias=use_bias,
                               padding=padding, **opts)(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.SpatialDropout2D(0.1)(x)

    return x


def _residual_block(inputs, filters, strides=1):
    x = _conv_block(inputs, filters=filters, kernel_size=1, strides=strides)
    x = _conv_block(x, filters=filters, kernel_size=3, strides=strides)
    x = tf.keras.layers.Conv2D(filters=filters * 4, kernel_size=1, strides=strides, padding='same', **opts)(x)
    x = tf.keras.layers.BatchNormalization()(x)

    x = cbam_block(x, ratio=8)

    x = tf.keras.layers.Add()([x, inputs])
    x = tf.keras.layers.ReLU()(x)

    return x


def _merge_with_shortcut_(inputs, filters, strides):
    x = _conv_block(inputs, filters=filters, kernel_size=1, strides=strides)
    x = _conv_block(x, filters=filters, kernel_size=3, strides=1)
    x = tf.keras.layers.Conv2D(filters=filters * 4, kernel_size=1, strides=1, padding='same', **opts)(x)
    x = tf.keras.layers.BatchNormalization()(x)

    merge_shortcut = tf.keras.layers.Conv2D(filters=filters * 4, kernel_size=1, strides=strides, padding='same', **opts)(inputs)
    merge_shortcut = tf.keras.layers.BatchNormalization()(merge_shortcut)

    x = tf.keras.layers.Add()([x, merge_shortcut])
    x = tf.keras.layers.ReLU()(x)

    return x


def _resnet_block(x, filters, repetition, strides):
    x = _merge_with_shortcut_(x, filters=filters, strides=strides)
    for r in range(repetition - 1):
        x = _residual_block(x, filters=filters)

    return x


def res_net_se():
    inputs = tf.keras.layers.Input(shape=(32, 32, 10))

    x = _conv_block(inputs, filters=64, kernel_size=(3, 3), strides=(1, 1))
    x = tf.keras.layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='same')(x)

    x = _resnet_block(x, filters=64, repetition=3, strides=1)
    x = _resnet_block(x, filters=128, repetition=4, strides=2)
    x = _resnet_block(x, filters=256, repetition=6, strides=2)
    x = _resnet_block(x, filters=512, repetition=3, strides=2)

    x = tf.keras.layers.GlobalAvgPool2D()(x)

    num_classes = 17
    outputs = tf.keras.layers.Dense(num_classes, activation='softmax', **opts)(x)

    resnet_50 = tf.keras.models.Model(inputs, outputs, name='RESNET50_SE')

    return resnet_50



if __name__ == '__main__':
    resnet_50_model = res_net_se()
    resnet_50_model.summary()
