import os
import tensorflow
import tensorflow as tf
from tensorflow.keras.regularizers import l2

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
devises = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(devises[0], True)


def _conv_block(x,
                filters,
                kernel_size,
                strides=1,
                use_bias=False,
                kernel_initializer='he_normal',
                kernel_regularizer=l2(.0001),
                padding='same'):
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.SpatialDropout2D(0.2)(x)
    x = tf.keras.layers.Conv2D(filters=filters,
                               kernel_size=kernel_size,
                               strides=strides,
                               use_bias=use_bias,
                               kernel_initializer=kernel_initializer,
                               kernel_regularizer=kernel_regularizer,
                               padding=padding)(x)
    return x


def _dense_block(r, input_tensor, num_block=4, growth_rate=16):
    for i in range(r):
        x = _conv_block(input_tensor, filters=num_block * growth_rate, kernel_size=(1, 1))
        x = _conv_block(x, filters=growth_rate, kernel_size=(3, 3))
        input_tensor = tf.keras.layers.Concatenate()([input_tensor, x])
    return input_tensor


def transition_layer(x, dim=0.5):
    filters = int(tf.keras.backend.int_shape(x)[-1] * dim)
    x = _conv_block(x, filters=filters, kernel_size=(1, 1))
    x = tf.keras.layers.AvgPool2D(pool_size=(2, 2), strides=(2, 2), padding='same')(x)
    return x


def dense_net():
    inputs = tf.keras.layers.Input(shape=(128, 128, 3))

    x = tensorflow.keras.layers.Conv2D(filters=32,
                                       kernel_size=(7, 7),
                                       strides=(2, 2),
                                       use_bias=False,
                                       padding='same',
                                       kernel_initializer='he_normal',
                                       kernel_regularizer=l2(.0001),
                                       name='conv')(inputs)
    x = tf.keras.layers.BatchNormalization(name='conv_bn')(x)
    x = tf.keras.layers.ReLU(name='conv_relu')(x)
    x = tf.keras.layers.MaxPool2D(pool_size=(3, 3), strides=(2, 2), padding='same', name='conv_max_pool')(x)

    r = 6, 12, 24, 16
    dense_block = None
    for i in r:
        dense_block = _dense_block(i, x, num_block=4, growth_rate=16)
        x = transition_layer(dense_block, dim=0.5)

    x = tensorflow.keras.layers.GlobalAveragePooling2D()(dense_block)

    x = tf.keras.layers.Dense(100, kernel_regularizer=l2(.0001))(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.Dropout(.2)(x)

    output = tf.keras.layers.Dense(4, activation='softmax')(x)

    model = tf.keras.models.Model(inputs, output)

    return model


if __name__ == "__main__":
    dense_net_model = dense_net()
    dense_net_model.summary()
