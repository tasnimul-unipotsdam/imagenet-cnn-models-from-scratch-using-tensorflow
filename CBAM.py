import tensorflow as tf
import tensorflow.keras.backend as k

def _channel_attention_(inputs, ratio=8):
    channel = inputs.shape()[-1]

    shared_layer1 = tf.keras.layers.Dense(channel // ratio, activation="relu", kernel_initializer="he_normal", use_bias=True, bias_initializer="zeros")
    shared_layer2 =  tf.keras.layers.Dense(channel, activation="relu", kernel_initializer="he_normal", use_bias=True, bias_initializer="zeros")

    x1 = tf.keras.layers.GlobalAveragePooling2D()(inputs)
    x1 = tf.keras.layers.Reshape((1, 1, channel))(x1)
    x1 = shared_layer1(x1)
    x1 = shared_layer2(x1)

    x2 = tf.keras.layers.GlobalMaxPooling2D()(inputs)
    x2 = tf.keras.layers.Reshape((1, 1, channel))(x2)
    x2 = shared_layer1(x2)
    x2 = shared_layer2(x2)

    x = tf.keras.layers.Add([x1, x2])
    x = tf.keras.layers.Activation("sigmoid")(x)

    x = tf.keras.layers.Multiply([inputs, x])

    return x



def _spatial_attention_(inputs):
    kernel_size = 7

    x1 = tf.keras.layers.Lambda(lambda x: k.mean(x, axis=3, keepdims=True))(inputs)
    x2 = tf.keras.layers.Lambda(lambda x: k.max(x, axis=3, keepdims=True))(inputs)

    concat = tf.keras.layers.Concatenate([x1, x2])
    conv = tf.keras.layers.Conv2D(1, kernel_size=kernel_size, strides=1, activation="sigmoid", padding="same", kernel_initializer="he_normal", use_bias=False)(concat)
    feature = tf.keras.layers.Multiply([inputs, conv])
    return feature

def cbam_block(inputs, ratio=8):

    channel_attention = _channel_attention_(inputs, ratio)
    spatial_attention = _spatial_attention_(channel_attention)
    return spatial_attention

