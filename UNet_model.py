import tensorflow as tf


def u_net():
    inputs = tf.keras.Input(shape=(128, 128, 3))
    conv1 = tf.keras.layers.Conv2D(64, (3, 3), activation="relu", padding="same")(inputs)
    conv1 = tf.keras.layers.Conv2D(64, (3, 3), activation="relu", padding="same")(conv1)
    pool1 = tf.keras.layers.MaxPool2D()(conv1)

    conv2 = tf.keras.layers.Conv2D(128, (3, 3), activation="relu", padding="same")(pool1)
    conv2 = tf.keras.layers.Conv2D(128, (3, 3), activation="relu", padding="same")(conv2)
    pool2 = tf.keras.layers.MaxPool2D()(conv2)

    conv3 = tf.keras.layers.Conv2D(256, (3, 3), activation="relu", padding="same")(pool2)
    conv3 = tf.keras.layers.Conv2D(256, (3, 3), activation="relu", padding="same")(conv3)
    pool3 = tf.keras.layers.MaxPool2D()(conv3)

    conv4 = tf.keras.layers.Conv2D(512, (3, 3), activation="relu", padding="same")(pool3)
    conv4 = tf.keras.layers.Conv2D(512, (3, 3), activation="relu", padding="same")(conv4)

    con5 = tf.keras.layers.concatenate(
        [tf.keras.layers.Conv2DTranspose(256, (2, 2), strides=(2, 2), padding="same")(conv4), conv3], axis=3)
    conv5 = tf.keras.layers.Conv2D(256, (3, 3), activation="relu", padding="same")(con5)
    conv5 = tf.keras.layers.Conv2D(256, (3, 3), activation="relu", padding="same")(conv5)

    con6 = tf.keras.layers.concatenate(
        [tf.keras.layers.Conv2DTranspose(128, (2, 2), strides=(2, 2), padding="same")(conv5), conv2], axis=3)
    conv6 = tf.keras.layers.Conv2D(128, (3, 3), activation="relu", padding="same")(con6)
    conv6 = tf.keras.layers.Conv2D(128, (3, 3), activation="relu", padding="same")(conv6)

    con7 = tf.keras.layers.concatenate(
        [tf.keras.layers.Conv2DTranspose(64, (2, 2), strides=(2, 2), padding="same")(conv6), conv1], axis=3)
    conv7 = tf.keras.layers.Conv2D(64, (3, 3), activation="relu", padding="same")(con7)
    conv7 = tf.keras.layers.Conv2D(64, (3, 3), activation="relu", padding="same")(conv7)

    conv8 = tf.keras.layers.Conv2D(1, (1, 1), activation="sigmoid")(conv7)

    model = tf.keras.Model(inputs, conv8, name='U-Net_model')
    return model


if __name__ == "__main__":
    u_net_model = u_net()
    u_net_model.summary()
