import os
import tensorflow
import tensorflow as tf
from tensorflow.keras.regularizers import l2

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
devises = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(devises[0], True)


def _conv_block(x,
                filters,
                kernel_size,
                strides,
                use_bias=False,
                kernel_initializer='he_normal',
                kernel_regularizer=l2(.0001),
                padding='same'):
    x = tf.keras.layers.Conv2D(filters=filters,
                               kernel_size=kernel_size,
                               strides=strides,
                               use_bias=use_bias,
                               kernel_initializer=kernel_initializer,
                               kernel_regularizer=kernel_regularizer,
                               padding=padding)(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.SpatialDropout2D(0.2)(x)

    return x


def _residual_block(inputs, filters, strides=1):
    x = _conv_block(inputs, filters=filters, kernel_size=1, strides=strides)
    x = _conv_block(x, filters=filters, kernel_size=3, strides=strides)
    x = tf.keras.layers.Conv2D(filters=filters * 4, kernel_size=1, strides=strides, padding='same')(x)
    x = tf.keras.layers.BatchNormalization()(x)

    x = tf.keras.layers.Add()([x, inputs])
    x = tf.keras.layers.ReLU()(x)

    return x


def _merge_with_shortcut_(inputs, filters, strides):
    x = _conv_block(inputs, filters=filters, kernel_size=1, strides=strides)
    x = _conv_block(x, filters=filters, kernel_size=3, strides=1)
    x = tf.keras.layers.Conv2D(filters=filters * 4, kernel_size=1, strides=1, padding='same')(x)
    x = tf.keras.layers.BatchNormalization()(x)

    merge_shortcut = tf.keras.layers.Conv2D(filters=filters * 4, kernel_size=1, strides=strides, padding='same')(inputs)
    merge_shortcut = tf.keras.layers.BatchNormalization()(merge_shortcut)

    x = tf.keras.layers.Add()([x, merge_shortcut])
    x = tf.keras.layers.ReLU()(x)

    return x


def _resnet_block(x, filters, repetition, strides):
    x = _merge_with_shortcut_(x, filters=filters, strides=strides)
    for r in range(repetition - 1):
        x = _residual_block(x, filters=filters)

    return x


def res_net():
    inputs = tf.keras.layers.Input(shape=(128, 128, 3))

    x = _conv_block(inputs, filters=64, kernel_size=(7, 7), strides=(2, 2))
    x = tf.keras.layers.MaxPool2D(pool_size=(3, 3), strides=(2, 2), padding='same')(x)

    x = _resnet_block(x, filters=64, repetition=3, strides=1)
    x = _resnet_block(x, filters=128, repetition=4, strides=2)
    x = _resnet_block(x, filters=256, repetition=6, strides=2)
    x = _resnet_block(x, filters=512, repetition=3, strides=2)

    x = tf.keras.layers.GlobalAvgPool2D()(x)

    num_classes = 4
    outputs = tf.keras.layers.Dense(num_classes, activation='softmax')(x)

    resnet_50 = tf.keras.models.Model(inputs, outputs, name='RESNET_50')

    return resnet_50


if __name__ == '__main__':
    resnet_50_model = res_net()
    resnet_50_model.summary()
