import os
import tensorflow as tf

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
devises = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(devises[0], True)

opts = {
    'kernel_initializer': tf.keras.initializers.he_uniform(),
    'bias_initializer': tf.keras.initializers.zeros()
}


def squeeze_excitation(x, ratio=16, **opt):
    opt = dict(opt, kernel_regularizer=tf.keras.regularizers.l2(0.0001))
    filters = x.get_shape()[-1]  # num channels (B, H, W, C)

    se = tf.keras.layers.GlobalAveragePooling2D()(x)
    se = tf.keras.layers.Reshape((1, 1, filters))(se)
    se = tf.keras.layers.Dense(filters // ratio, use_bias=False,**opts)(se)
    se = tf.keras.layers.ReLU()(se)
    se = tf.keras.layers.Dense(filters, **opts)(se)
    se = tf.keras.layers.Activation('sigmoid')(se)
    se = tf.keras.layers.Reshape((1, 1, filters))(se)
    x = tf.keras.layers.Multiply()([x, se])
    return x


def _conv_block(x,
                filters,
                kernel_size,
                strides,
                use_bias=False,
                padding='same'):
    x = tf.keras.layers.Conv2D(filters=filters,
                               kernel_size=kernel_size,
                               strides=strides,
                               use_bias=use_bias,
                               padding=padding, **opts)(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.SpatialDropout2D(0.1)(x)

    return x


def _residual_block(inputs, filters, strides=1):
    x = _conv_block(inputs, filters=filters, kernel_size=1, strides=strides)
    x = _conv_block(x, filters=filters, kernel_size=3, strides=strides)
    x = tf.keras.layers.Conv2D(filters=filters * 4, kernel_size=1, strides=strides, padding='same', **opts)(x)
    x = tf.keras.layers.BatchNormalization()(x)

    x = squeeze_excitation(x)

    x = tf.keras.layers.Add()([x, inputs])
    x = tf.keras.layers.ReLU()(x)

    return x


def _merge_with_shortcut_(inputs, filters, strides):
    x = _conv_block(inputs, filters=filters, kernel_size=1, strides=strides)
    x = _conv_block(x, filters=filters, kernel_size=3, strides=1)
    x = tf.keras.layers.Conv2D(filters=filters * 4, kernel_size=1, strides=1, padding='same', **opts)(x)
    x = tf.keras.layers.BatchNormalization()(x)

    merge_shortcut = tf.keras.layers.Conv2D(filters=filters * 4, kernel_size=1, strides=strides, padding='same', **opts)(inputs)
    merge_shortcut = tf.keras.layers.BatchNormalization()(merge_shortcut)

    x = tf.keras.layers.Add()([x, merge_shortcut])
    x = tf.keras.layers.ReLU()(x)

    return x


def _resnet_block(x, filters, repetition, strides):
    x = _merge_with_shortcut_(x, filters=filters, strides=strides)
    for r in range(repetition - 1):
        x = _residual_block(x, filters=filters)

    return x


def res_net_se():
    inputs = tf.keras.layers.Input(shape=(32, 32, 10))

    x = _conv_block(inputs, filters=64, kernel_size=(3, 3), strides=(1, 1))
    x = tf.keras.layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='same')(x)

    x = _resnet_block(x, filters=64, repetition=3, strides=1)
    x = _resnet_block(x, filters=128, repetition=4, strides=2)
    x = _resnet_block(x, filters=256, repetition=6, strides=2)
    x = _resnet_block(x, filters=512, repetition=3, strides=2)

    x = tf.keras.layers.GlobalAvgPool2D()(x)

    num_classes = 17
    outputs = tf.keras.layers.Dense(num_classes, activation='softmax', **opts)(x)

    resnet_50 = tf.keras.models.Model(inputs, outputs, name='RESNET50_SE')

    return resnet_50



if __name__ == '__main__':
    resnet_50_model = res_net_se()
    resnet_50_model.summary()
